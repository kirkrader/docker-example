/* Copyright (c) 2017 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.demo;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Hello service Spring Boot application.
 */
@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackageClasses = { HelloController.class })
public class Application {

  private static final Logger LOGGER          = LogManager
          .getLogger(Application.class);

  /**
   * Key for the configured message string as defined in the configuration file
   * read into the value returned by {@link #getProperties()}.
   */
  public static final String  MESSAGE_KEY     = "message";

  static final String         DEFAULT_MESSAGE = "Hello, world!";

  private static class PropertiesHolder {

    private static final Properties properties = loadProperties();

    @SuppressFBWarnings(value = { "DMI_HARDCODED_ABSOLUTE_FILENAME" },
            justification = "Hard-coded config file path driven by contents of Dockerfile")
    private static Properties loadProperties() {

      Properties properties = new Properties();

      try (FileInputStream stream = new FileInputStream(
              new File("/opt/config/config.properties"))) {

        properties.load(stream);

      } catch (IOException e) {

        LOGGER.error("error loading properties file", e);
        properties.put(MESSAGE_KEY, DEFAULT_MESSAGE);

      }

      return properties;

    }

    public static Properties getProperties() {

      return properties;

    }

  }

  /**
   * Get the configuration file Properties.
   *
   * @return The configuration Properties.
   */
  public static Properties getProperties() {

    return PropertiesHolder.getProperties();

  }

  /**
   * Value for LogContextKeys.APPLICATION_ID.
   */
  public static final String APPLICATION_ID = "hello-service";

  /**
   * Hello service Spring Boot application entry point.
   *
   * @param args
   *          Command-line arguments.
   */
  public static void main(String[] args) {

    SpringApplication.run(Application.class, args);

  }

}
