#!/bin/bash

docker run \
  --name hello-service \
  -p 8080:8080 \
  --mount type=bind,source=${PWD}/ci/config,target=/opt/config \
  hello-service
