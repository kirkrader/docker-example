/* Copyright (c) 2017 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.demo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.io.InputStream;
import org.apache.logging.log4j.CloseableThreadContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Simple "hello world" service to test build configuration.
 */
@RestController
@EnableAutoConfiguration
@Api(basePath = "/hello", description = "Hello, world!")
public class HelloController {

  private static final Logger LOGGER;

  static {

    LOGGER = LogManager.getLogger(HelloController.class);

  }

  /**
   * Place-holder entry point to test build configuration.
   *
   * @return "Hello, world!"
   */
  @GetMapping("/hello")
  @ApiOperation(value = "Default entry point",
          notes = "Place-holder entry point to test build configuration.")
  public String index() {

    try {

      return Application.getProperties().getProperty(Application.MESSAGE_KEY);

    } catch (Exception e) {

      // note that we deliberately do not allow recoverable errors to propagate
      // as status code 500 ... if the controller is responding, always return
      // status code 200 and indicate the outcome in the response payload!
      LOGGER.error("something bad happened", e);
      return ">>> ERROR <<<";

    }
  }

  /**
   * Upload a file.
   *
   * @param file
   *          The request parameter containing the uploaded file's contents and
   *          metadata
   *
   * @return A diagnostic message
   */
  @PostMapping("/upload")
  @ApiOperation(value = "Upload a file",
          notes = "POST handler for multipart/form-data containing the contents of a file")
  public String upload(@RequestParam("file") MultipartFile file) {

    try {

      final String originalFilename = file.getOriginalFilename();
      final String contentType = file.getContentType();
      long total = readFile(file);
      LOGGER.trace("read " + total + " bytes");

      if (total != file.getSize()) {

        throw new Exception(
                "expected " + file.getSize() + " bytes, got " + total);

      }

      return originalFilename + " (" + contentType + ")";

    } catch (Exception e) {

      LOGGER.error("error uploading file", e);
      return ">>> ERROR <<<";

    }
  }

  /**
   * Read the contents of the uploaded file using the supplied input stream.
   *
   * @param file
   *          The file stream
   *
   * @return The number of bytes read from the file.
   */
  public long readFile(MultipartFile file) throws IOException {

    final byte[] buffer = new byte[1024];
    long total = 0;

    try (final InputStream stream = file.getInputStream()) {

      int actual;

      while ((actual = stream.read(buffer, 0, buffer.length)) > 0) {

        LOGGER.trace("buffered " + actual + " bytes");
        total += actual;

      }
    }

    return total;
  }

}
