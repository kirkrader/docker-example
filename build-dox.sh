mvn clean package -Ddoxygen -DskipTests
pushd target/doxygen/latex
make
popd
cp target/doxygen/latex/refman.pdf services.pdf
