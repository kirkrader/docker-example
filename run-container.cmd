@REM set %DIR% to the absolute path of the directory where this script resides
set DIR=%~dp0

@REM bind /opt/config in the container's file-system to ci\config relative to %DIR%
docker run --name hello-service -p 8080:8080 --mount type=bind,source=%DIR%ci\config\,target=/opt/config hello-service
