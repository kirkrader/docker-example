/* Copyright (c) 2017 Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package us.rader.demo;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URL;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Integration tests for HelloController.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = { HelloController.class })
@AutoConfigureMockMvc
public final class HelloControllerIT {

  /**
   * TCP/IP port on which server instance is run.
   */
  @LocalServerPort
  private int              port;

  /**
   * Base URL for servlet instance.
   */
  private URL              base;

  /**
   * REST template for integration tests.
   */
  @Autowired
  private TestRestTemplate template;

  /**
   * Set `base` before invoking test method.
   */
  @Before
  public void before() throws Exception {

    base = new URL("http://localhost:" + port + "/hello");

  }

  /**
   * Demonstrate unit testing for a Spring controller.
   *
   * This shows that integration tests can look exactly like unit tests, except
   * assume the existence of some well-defined environment, e.g. as controlled
   * using Maven's failsafe plugin and its support for pre-integration-test and
   * post-integration test phases.
   */
  @Test
  public void indexIT() {

    final ResponseEntity<String> response = template
            .getForEntity(base.toString(), String.class);
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertThat(response.getBody(), equalTo("Hello, world!"));

  }
}
