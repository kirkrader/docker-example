Copyright &copy; Kirk Rader 2018. All rights reserved.

# us.rader.demo:docker-example

[Maven](http://maven.apache.org/) based multi-module [Spring Boot](https://projects.spring.io/spring-boot/) project with one child module. That child module implements a literal "Hello, world!" web service. This project also demonstrates building and deploying such services using [Docker](https://www.docker.com/) containers. See [Deploying Docker Containers](#deploying-docker-containers), below.

See <https://kirkrader.gitlab.io/docker-example/> for full documentation and release notes generated as part of the CI build process.

The _.gitlab-ci.yml_ file for this project uses _registry.gitlab.com/kirkrader/java-build-environment:latest_ whose source code can be found at <https://gitlab.com/kirkrader/java-build-environment>.

The _pom.xml_ for this project invokes automated code quality, static analysis, unit and integration tests. The software governance rules are defined by dependencies on _us.rader.swgov:java-code-quality:1.10_ and enforced using Maven plugins for [Checkstyle](http://checkstyle.sourceforge.net/), [FindBugs](http://findbugs.sourceforge.net/) and [Find Security Bugs](https://find-sec-bugs.github.io/). The source code for the _java-code-quality_ module can be found at <https://gitlab.com/kirkrader/java-code-quality>. Its binary dependency is resolved by the artifact repo at <https://packagecloud.io/parasaurolophus/snapshot/maven2>.

## Building and Running

### Build-Time Dependencies

The tool chain required to build this project consists of:

- JDK 8
- Maven
- Doxygen
- PlantUML
- Graphviz
- texlive
- Docker
- A few "core" build tools for your environment (e.g. the _build-essential_ package for Ubuntu, GNU _make_ via Homebrew for Mac OSX etc.)

All other dependencies are resolved using Maven at build time. To support Maven's use of the tool chain, the following environment variables must be set:

| Environment Variable | Value                                                 |
| -------------------- | ----------------------------------------------------- |
| `DOXYGEN_EXE`        | Absolute path of Doxygen executable file              |
| `PLANTUML_JAR`       | Absolute path of _plantuml.jar_                       |
| `GRAPHVIZ_DOT`       | Absolute path of Graphviz _dot_ executable file       |
| `DOCKER_EXE`         | Absolute path of _docker_ command-ine executable file |
| `DOCKER_COMPOSE_EXE` | Absolute path of _docker-compose_ executable file     |

### Run-Time Dependencies

The runtime platform requires only Docker. The Spring Boot application and its dependency on JRE 8 are fully containerized.

### Building and Deploying

To create Docker images and corresponding containers locally:

    mvn clean install -Ddoxygen -Dintegration-test
    docker run -d --name hello-service -p 8080:8080 --mount type=bind,source=%DIR%,target=/opt/config hello-service

Note that you may have to adjust the values of the `-p` parameters in the preceding `docker create ...` commands depending on which TCP/IP ports are free in your local environment. Note also that `%DIR%` is assumed to refer to the absolute path of the directory containing the application configuration file, _config.properties_.

> Using manually-supplied port mappings and bind mounts to pass in configuration files in this way supports multiple instances of a given service to exist concurrently in a single runtime environment but does not scale well to production scenarios. Container orchestration platforms like Kubernetes provide automation-friendly services for managing the configuration of containers in ways that support automatic scaling, fail-over etc.

You can build and test individual containers by cd'ing to a given module directory to build only the corresponding service.

    pushd hello-service
    mvn clean install
    popd
    docker run -d --name hello-service -p 8080:8080 --mount type=bind,source=%DIR%,target=/opt/config hello-service

Either way, once the _hell0-service_ container is running open <http://localhost:8080/> in a web browser.

You can access the Swagger documentation / UI at <http://localhost:8080/swagger-ui.html>.

(You will need to adjust the preceding URL's based on the actual port mapping you specified when creating your local containers.)

When finished:

    docker stop hello-service
    docker rm hello-service
    docker rmi hello-service

If all you want is to test locally by manually invoking Spring Boot executable JAR's:

    mvn clean package
    java -jar hello-service/target/hello-service-1.0-SNAPSHOT.jar


The generated POM files implicitly invoke [JUnit](http://junit.org) based unit tests using Maven's _surefire_ plugin. In addition, they include a profile to invoke integration tests using Maven's _failsafe_ plugin when activated by the `integration-test` Java property.

In other words, a command line like

    mvn install

implicitly invokes unit tests. The build will break if any unit tests fail.

On the other hand,

    mvn install -Dintegration-test

invokes both unit and integration tests. The build breaks if any unit or integration tests fail, including due to attempting to run integration tests outside of a suitably configured environment. Such explicit activation of integration tests is a key assumption made by any sensible CI build process.

To publish the Docker images to the default binary repository:

    # Note that you will need to adjust the Docker tag
    # specified in _pom.xml_ in order to deploy to your
    # own repository in this way. Otherwise you can build
    # the image locally using mvn install and then use
    # explict invocations of docker tag and docker push.
    mvn deploy

You can then delete the images from your local Docker repository and still invoke `docker create ...` as shown above. It will fetch the images from the remote Docker repository when creating the local containers.

The `-Ddoxygen` option to the `mvn ...` build invocations shown above will use Doxygen to generate highly enriched, Javadoc-style documentation in both HTML and LaTeX formats. The LaTeX output will include a script for publishing to PDF.

The HTML documentation is rooted at _target/doxygen/html/index.html_.

To create a PDF file:

    mvn clean validate -Ddoxygen
    pushd target/doxygen/latex
    make
    popd

The PDF output will be in _target/doxygen/latex/refman.pdf_.
